# Project Hadit

## Projeto

Projeto que realiza uma busca em um recurso público do Reddit, mais especificamente no tema de tecnologia.
Em uma tela você consegue visualizar as postagens realizadas e selecionando uma, você consegue ver 
todos os comentários já feitos sobre ela.

### pré-requisitos

Tenha o Android Studio instalado e suas dependências.

### Instalando

Basta abrir o projeto com o Android Studio e apertar no botão de rodar. 

Se tiver um dispositivo conectado ele vai rodar o aplicativo ou irá iniciar um emulador. 

## Tecnologias utilizadas dentro do projeto

 - Retrofit
 - Coroutines
 - Koin
 - Kotlin
 - JetPack
 - Robolectric
 - Mockk
 - Mockito
 
## Arquitetura do projeto
 - MVVM

## Rodando os testes

Com o android studio aberto no projeto, clique com o botão direito na pasta "br.com.aguiar.matheus.hadit (test)" procure a opção "Run test in hadit" e clique nela.

