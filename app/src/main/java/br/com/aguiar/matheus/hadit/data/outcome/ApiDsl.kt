package br.com.aguiar.matheus.hadit.data.outcome

suspend fun <T> requestApi(f: suspend () -> Outcome<T>): Outcome<T> {
    return try {
        f()
    } catch (e: Exception) {
        Outcome.Error(e)
    }
}