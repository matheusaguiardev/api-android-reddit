package br.com.aguiar.matheus.hadit.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.ui.adapter.CommentAdapter
import br.com.aguiar.matheus.hadit.ui.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {

    companion object {
        const val POST_ID = "POST_ID"
    }

    private val viewModel: MainViewModel by viewModel()

    private val adapter by lazy { CommentAdapter(this) }

    val postId by lazy { this.intent.getStringExtra(POST_ID) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        viewModel.comments().observe(this, Observer(::commentsObserver))
        viewModel.processingFlag().observe(this, Observer(::asyncTaskObserver))
    }

    override fun onStart() {
        super.onStart()

        postId?.let {
            viewModel.fetchCommentsFrom(it)
        } ?: run {
            Toast.makeText(this, "Não consegui identificar a url desse post", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun commentsObserver(commentList: List<RedditComments>?){
        commentList?.let{
            if (it.isNotEmpty()) {
                it.first().postComments.dataComment?.first()?.commentsChildren.apply {
                    textView_title.text = this?.titleComment
                    textView_author.text = getString(R.string.txt_author, this?.author)
                    textView_body.text = this?.commentPost
                    textView_like.text = getString(R.string.txt_like, this?.getVotesPositives())
                    textView_dislike.text = getString(R.string.txt_dislike, this?.getVotesNegatives())
                }
                it.last().postComments.apply {
                    setUpList()
                    adapter.dataSource = this.dataComment ?: emptyList()
                }
            }
        }
    }

    private fun noContentState(msg: String?) {
        msg?.let { message ->
            textView_error_msg.visibility = View.VISIBLE
            textView_error_msg.text = message
            comments_recycler.visibility = View.GONE
            header_container.visibility = View.GONE
        } ?: run {
            comments_recycler.visibility = View.VISIBLE
            header_container.visibility = View.VISIBLE
            textView_error_msg.visibility = View.GONE
        }
    }

    private fun asyncTaskObserver(running: Boolean) {
        if(running) {
            comments_recycler.visibility = View.GONE
            header_container.visibility = View.GONE
            textView_error_msg.visibility = View.GONE
            progress_detail.visibility = View.VISIBLE
        } else {
            progress_detail.visibility = View.GONE
            noContentState(viewModel.errorMsg().value)
        }
    }

    private fun setUpList(){
        if(comments_recycler.adapter != null) return
        comments_recycler.layoutManager = LinearLayoutManager(this)
        comments_recycler.adapter = adapter
    }
}