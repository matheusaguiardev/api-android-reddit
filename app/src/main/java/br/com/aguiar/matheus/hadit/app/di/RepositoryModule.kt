package br.com.aguiar.matheus.hadit.app.di

import br.com.aguiar.matheus.hadit.data.reddit.RedditRepositoryImpl
import br.com.aguiar.matheus.hadit.domain.RedditRepository
import org.koin.dsl.module

val repositoryModule = module {
    single<RedditRepository> { RedditRepositoryImpl(get()) }
}