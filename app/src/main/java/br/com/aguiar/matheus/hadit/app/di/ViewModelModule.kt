package br.com.aguiar.matheus.hadit.app.di

import br.com.aguiar.matheus.hadit.ui.viewmodel.MainViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get(), Dispatchers.Main) }
}