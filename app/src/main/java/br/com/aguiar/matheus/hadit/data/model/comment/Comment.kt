package br.com.aguiar.matheus.hadit.data.model.comment

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("title") val titleComment: String?,
    @SerializedName("author") val author: String?,
    @SerializedName("ups") val votesPositive: Int?,
    @SerializedName("body") val commentPost: String?

) {

    fun getVotesPositives(): Int {
        votesPositive?.let {
            return if(it >= 0) {
                it
            } else {
                0
            }
        } ?: run { return 0 }
    }

    fun getVotesNegatives(): Int {
        votesPositive?.let {
            return if(it < 0) {
                it
            } else {
                0
            }
        } ?: run { return 0 }
    }

}