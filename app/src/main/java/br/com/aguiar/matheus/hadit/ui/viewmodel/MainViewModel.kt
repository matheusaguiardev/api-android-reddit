package br.com.aguiar.matheus.hadit.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.outcome.Outcome
import br.com.aguiar.matheus.hadit.domain.RedditRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MainViewModel(
    private val repository: RedditRepository,
    private val dispatcher: CoroutineDispatcher
) : ViewModel(), CoroutineScope {

    companion object {
        private const val NOT_FOUND = "Nenhum dado encontrado"
    }

    private val _job = Job()
    private var _postJob: Job? = null
    private var _commentsJob: Job? = null

    override val coroutineContext: CoroutineContext = dispatcher + _job

    private val _posts = MutableLiveData<List<DataChildren>>()
    fun posts(): LiveData<List<DataChildren>> = _posts

    private val _comments = MutableLiveData<List<RedditComments>>()
    fun comments(): LiveData<List<RedditComments>> = _comments

    private val _errorMsg = MutableLiveData<String?>()
    fun errorMsg(): LiveData<String?> = _errorMsg

    private val _processingFlag = MutableLiveData<Boolean>()
    fun processingFlag(): LiveData<Boolean> = _processingFlag

    fun fetchPosts() {
        if (_postJob?.isActive == true) {
            return
        }
        _postJob = launch { fetchPostsAsync() }
    }

    fun fetchCommentsFrom(url: String) {
        if (_commentsJob?.isActive == true) {
            return
        }
        _commentsJob = launch { fetchCommentsAsync(url) }
    }

    private suspend fun fetchPostsAsync() = withContext(dispatcher) {
        _processingFlag.value = true
        val response = repository.getHome()
        _processingFlag.value = false
        _posts.value = when (response) {
            is Outcome.Success -> {
                _errorMsg.value = null
                response.data
            }
            is Outcome.Error -> {
                _errorMsg.value = NOT_FOUND
                emptyList()
            }
        }
    }

    private suspend fun fetchCommentsAsync(url: String) = withContext(dispatcher) {
        _processingFlag.value = true
        val response = repository.getComments(url)
        _processingFlag.value = false
        _comments.value = when (response) {
            is Outcome.Success -> {
                _errorMsg.value = null
                response.data
            }
            is Outcome.Error -> {
                _errorMsg.value = response.exception.message ?: NOT_FOUND
                emptyList()
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        _postJob?.cancel()
        _commentsJob?.cancel()
        _job.cancel()
    }

}