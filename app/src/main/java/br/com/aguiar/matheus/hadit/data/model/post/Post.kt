package br.com.aguiar.matheus.hadit.data.model.post

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("title") val title: String?,
    @SerializedName("permalink") val details: String?,
    @SerializedName("url") val link: String?,
    @SerializedName("author") val author: String?,
    @SerializedName("num_comments") val numComments: Int?,
    @SerializedName("created") val created: Long?,
    @SerializedName("selftext") val authorText: String?
)