package br.com.aguiar.matheus.hadit.data.model.comment

import com.google.gson.annotations.SerializedName

data class DataComment(
    @SerializedName("children") val dataComment: List<CommentChildren>?
)