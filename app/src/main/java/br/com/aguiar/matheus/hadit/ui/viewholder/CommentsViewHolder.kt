package br.com.aguiar.matheus.hadit.ui.viewholder

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.comment.Comment
import kotlinx.android.synthetic.main.comments_view_holder.view.*

class CommentsViewHolder(
    itemView: View,
    private val context: Context
): RecyclerView.ViewHolder(itemView) {

    fun bind(comment: Comment?) {
        itemView.textView_body.text = comment?.commentPost ?: "No comment :D"
        itemView.textView_like.text = context.getString(R.string.txt_like, comment?.getVotesPositives() ?: 0)
        itemView.textView_dislike.text = context.getString(R.string.txt_dislike, comment?.getVotesNegatives() ?: 0)
        itemView.textView_author.text = context.getString(R.string.txt_author, comment?.author ?: 0)
    }

}