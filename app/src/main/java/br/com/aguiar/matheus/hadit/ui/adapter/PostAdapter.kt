package br.com.aguiar.matheus.hadit.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.model.post.Post
import br.com.aguiar.matheus.hadit.ui.viewholder.PostsViewHolder

class PostAdapter(
    private val callback: (String) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<PostsViewHolder>() {

    private var dataSource: List<DataChildren> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_holder, parent, false)
        return PostsViewHolder(view, callback)
    }

    override fun getItemCount() = dataSource.size

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.bind(dataSource[position].data)
    }

    fun setList(list: List<DataChildren>) {
        dataSource = list
        notifyDataSetChanged()
    }

}