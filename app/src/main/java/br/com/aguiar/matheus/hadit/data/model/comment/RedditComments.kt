package br.com.aguiar.matheus.hadit.data.model.comment

import com.google.gson.annotations.SerializedName

data class RedditComments(
    @SerializedName("data") val postComments: DataComment
)