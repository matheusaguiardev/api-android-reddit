package br.com.aguiar.matheus.hadit.domain

import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.outcome.Outcome

interface RedditRepository {

 suspend fun getHome(): Outcome<List<DataChildren>>

 suspend fun getComments(url: String): Outcome<List<RedditComments>>

}