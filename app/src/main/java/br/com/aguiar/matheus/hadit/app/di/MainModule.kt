package br.com.aguiar.matheus.hadit.app.di

val myModule = listOf(
    remoteModule,
    viewModelModule,
    repositoryModule
)