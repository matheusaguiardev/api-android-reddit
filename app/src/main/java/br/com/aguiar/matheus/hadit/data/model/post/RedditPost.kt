package br.com.aguiar.matheus.hadit.data.model.post

import com.google.gson.annotations.SerializedName

data class RedditPost(
    @SerializedName("data") val postData: PostData
)