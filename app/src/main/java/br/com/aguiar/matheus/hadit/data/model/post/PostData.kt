package br.com.aguiar.matheus.hadit.data.model.post

import com.google.gson.annotations.SerializedName

data class PostData (
    @SerializedName("children") val children: List<DataChildren>?
)