package br.com.aguiar.matheus.hadit.ui.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.post.Post
import kotlinx.android.synthetic.main.card_view_holder.view.*

class PostsViewHolder(
    itemView: View,
    private val callback: (String) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    fun bind(post: Post?) {
        with(itemView) {
            titleTxt.text = context.getString(R.string.txt_titulo_post, post?.title ?: "Sem título")
            descriptionTxt.text = post?.authorText ?: "Sem texto"
            amountComments.text = context.getString(R.string.txt_amount_comments, post?.numComments ?: 0)
            ownerTxt.text = context.getString(R.string.txt_owner_post, post?.author ?: "Anonimo")

            post?.details?.let { link -> setOnClickListener { callback.invoke(link) } }
        }
    }

}