package br.com.aguiar.matheus.hadit.data.reddit

import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.outcome.Outcome
import br.com.aguiar.matheus.hadit.data.outcome.requestApi
import br.com.aguiar.matheus.hadit.domain.RedditRepository
import java.io.IOException

class RedditRepositoryImpl(
    private val api: RedditRemote
) : RedditRepository {

    companion object {
        const val DATA_NOT_FOUND = "Infelizmente, não foi possível encontrar seus posts."
    }

    override suspend fun getHome(): Outcome<List<DataChildren>> {
        return requestApi { fetchDataHome() }
    }

    override suspend fun getComments(url: String): Outcome<List<RedditComments>> {
        return requestApi { fetchDataComments(url) }
    }

    private suspend fun fetchDataHome(): Outcome<List<DataChildren>> {
        val result = api.homeAsync().await()

        return when (result.code()) {
            200 -> {
                val response = result.body()?.postData?.children
                response?.let {
                    Outcome.Success(it)
                } ?: run {
                    Outcome.Error(
                        IOException(DATA_NOT_FOUND),
                        httpCode = result.code()
                    )
                }
            }
            else -> {
                Outcome.Error(
                    IOException(DATA_NOT_FOUND),
                    httpCode = result.code()
                )
            }
        }
    }

    private suspend fun fetchDataComments(url: String): Outcome<List<RedditComments>> {
        val result = api.commentsAsync(url.plus(".json")).await()

        return when (result.code()) {
            200 -> {
                val response = result.body()
                response?.let {
                    Outcome.Success(it)
                } ?: run {
                    Outcome.Error(
                        IOException(DATA_NOT_FOUND),
                        httpCode = result.code()
                    )
                }
            }
            else -> {
                Outcome.Error(
                    IOException(DATA_NOT_FOUND),
                    httpCode = result.code()
                )
            }
        }
    }

}