package br.com.aguiar.matheus.hadit.data.reddit

import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.RedditPost
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface RedditRemote {

    @GET("/r/technology.json")
    fun homeAsync(): Deferred<Response<RedditPost>>

    @GET
    fun commentsAsync(@Url url: String): Deferred<Response<List<RedditComments>>>
}