package br.com.aguiar.matheus.hadit.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.ui.adapter.PostAdapter
import br.com.aguiar.matheus.hadit.ui.detail.DetailActivity
import br.com.aguiar.matheus.hadit.ui.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), LifecycleOwner {

    companion object {
        private const val NUM_COLUMN = 1
    }

    private val viewModel: MainViewModel by viewModel()

    private val adapter by lazy { PostAdapter(::itemCallbackClick, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(viewModel) {
            posts().observe(this@MainActivity, Observer(::outcomeList))
            processingFlag().observe(this@MainActivity, Observer(::asyncTaskObserver))
        }

        viewModel.fetchPosts()
    }

    private fun outcomeList(list: List<DataChildren>) {
        adapter.setList(list)
        setUpList()
    }

    private fun setUpList() {
        if (postsList.adapter == null) {
            postsList.layoutManager = GridLayoutManager(this, NUM_COLUMN)
            postsList.adapter = adapter
        }
    }

    private fun asyncTaskObserver(running: Boolean) {
        if(running) {
            postsList.visibility = View.GONE
            progress_home.visibility = View.VISIBLE
        } else {
            postsList.visibility = View.VISIBLE
            progress_home.visibility = View.GONE
        }
    }

    private fun itemCallbackClick(url: String) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(DetailActivity.POST_ID, url)
        startActivity(intent)
    }

}