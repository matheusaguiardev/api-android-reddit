package br.com.aguiar.matheus.hadit.data.model.post

import com.google.gson.annotations.SerializedName

data class DataChildren(
    @SerializedName("data")  val data: Post?
)