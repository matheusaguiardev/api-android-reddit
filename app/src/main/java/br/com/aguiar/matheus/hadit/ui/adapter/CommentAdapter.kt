package br.com.aguiar.matheus.hadit.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.aguiar.matheus.hadit.R
import br.com.aguiar.matheus.hadit.data.model.comment.CommentChildren
import br.com.aguiar.matheus.hadit.ui.viewholder.CommentsViewHolder

class CommentAdapter(
    private val context: Context
): RecyclerView.Adapter<CommentsViewHolder>() {

    var dataSource: List<CommentChildren> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.comments_view_holder, parent, false)
        return CommentsViewHolder(view, context)
    }

    override fun getItemCount() = dataSource.size

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
       holder.bind(dataSource[position].commentsChildren)
    }
}