package br.com.aguiar.matheus.hadit.viewmodel

import android.os.Build
import androidx.lifecycle.LiveData
import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.outcome.Outcome
import br.com.aguiar.matheus.hadit.domain.RedditRepository
import br.com.aguiar.matheus.hadit.testhelper.commentList
import br.com.aguiar.matheus.hadit.testhelper.getOrAwaitValue
import br.com.aguiar.matheus.hadit.testhelper.postList
import br.com.aguiar.matheus.hadit.ui.viewmodel.MainViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1, Build.VERSION_CODES.LOLLIPOP, Build.VERSION_CODES.N])
@ExperimentalCoroutinesApi
class MainViewModelTest{

    private val repository = mock<RedditRepository>()
    private val testDispatcher: CoroutineDispatcher = TestCoroutineDispatcher()
    private val viewModel = MainViewModel(repository, testDispatcher)

    private lateinit var observerPosts: LiveData<List<DataChildren>>
    private lateinit var observerComments: LiveData<List<RedditComments>>
    private lateinit var observerLoading: LiveData<Boolean>
    private lateinit var observerErrorMsg: LiveData<String?>

    private val errorTest = "Nenhum dado encontrado"

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun clear() {
        Dispatchers.resetMain()
        stopKoin()
    }

    @Test fun `iniciar um request de postagens`() = runBlockingTest {
        `dado que eu tenha uma lista de postagens para baixar`()
        `quando eu realizar a solicitacao das postagens`()
        `entao devo me certificar que as postagens estao sendo definidas`()
        `entao a mensagem de erro deve ser nula`()
    }

    @Test fun `iniciar um request de postagens e um erro seja esperado`() = runBlockingTest {
        `dado que eu tenha uma lista de postagens para baixar e ocorrer um erro durante o processo`()
        `quando eu realizar a solicitacao das postagens`()
        `entao a mensagem de erro deve ser definida com a mensagem da exception`()
    }

    @Test fun `iniciar um request de comentarios`() = runBlockingTest {
        `dado que eu tenha uma lista de comentarios para baixar`()
        `quando eu realizar a solicitacao dos comentarios`()
        `entao devo me certificar que os comentarios estao sendo definidos como esperado`()
        `entao a mensagem de erro deve ser nula`()
    }

    @Test fun `iniciar um request de comentarios  e um erro seja esperado`() = runBlockingTest {
        `dado que eu tenha uma lista de comentariospara baixar e ocorrer um erro durante o processo`()
        `quando eu realizar a solicitacao dos comentarios`()
        `entao a mensagem de erro deve ser definida com a mensagem da exception`()
    }

    @Test fun `apresentar loading quando realizar uma requisicao das postagens`() = runBlockingTest{
        `dado que eu tenha uma lista de postagens para baixar`()
        `quando eu realizar a solicitacao das postagens`()
        `entao o loading deve estar definido como false no final da requisicao`()
    }

    @Test fun `apresentar loading quando realizar uma requisicao dos comentarios`() = runBlockingTest{
        `dado que eu tenha uma lista de comentarios para baixar`()
        `quando eu realizar a solicitacao dos comentarios`()
        `entao o loading deve estar definido como false no final da requisicao`()
    }

    /*************** DADO ******************/
    private suspend fun `dado que eu tenha uma lista de postagens para baixar`() {
        whenever(repository.getHome())
            .thenReturn(Outcome.Success(postList.postData.children!!))
    }

    private suspend fun `dado que eu tenha uma lista de postagens para baixar e ocorrer um erro durante o processo`() {
        whenever(repository.getHome())
            .thenReturn(Outcome.Error(IOException(errorTest)))
    }

    private suspend fun `dado que eu tenha uma lista de comentarios para baixar`() {
        whenever(repository.getComments(anyString()))
            .thenReturn(Outcome.Success(commentList))
    }

    private suspend fun `dado que eu tenha uma lista de comentariospara baixar e ocorrer um erro durante o processo`() {
        whenever(repository.getComments(anyString()))
            .thenReturn(Outcome.Error(IOException(errorTest)))
    }

    /*************** QUANDO ******************/
    private fun `quando eu realizar a solicitacao das postagens`() {
        viewModel.fetchPosts()
        observerPosts = viewModel.posts()
        observerLoading = viewModel.processingFlag()
        observerErrorMsg = viewModel.errorMsg()
    }

    private fun `quando eu realizar a solicitacao dos comentarios`() {
        viewModel.fetchCommentsFrom("www.reddit.com/r/")
        observerComments = viewModel.comments()
        observerLoading = viewModel.processingFlag()
        observerErrorMsg = viewModel.errorMsg()
    }

    /*************** ENTÃO ******************/
    private fun `entao devo me certificar que as postagens estao sendo definidas`() {
        Assert.assertTrue(observerPosts.getOrAwaitValue()?.isNotEmpty() ?: false)
    }

    private fun `entao devo me certificar que os comentarios estao sendo definidos como esperado`() {
        Assert.assertTrue(observerComments.getOrAwaitValue()?.isNotEmpty() ?: false)
    }

    private fun `entao o loading deve estar definido como false no final da requisicao`() {
        Assert.assertFalse(observerLoading.getOrAwaitValue() ?: true)
    }

    private fun `entao a mensagem de erro deve ser nula`() {
        Assert.assertTrue(observerErrorMsg.getOrAwaitValue() == null)
    }

    private fun `entao a mensagem de erro deve ser definida com a mensagem da exception`() {
        Assert.assertTrue(observerErrorMsg.getOrAwaitValue() == errorTest)
    }

}