package br.com.aguiar.matheus.hadit.testhelper

import br.com.aguiar.matheus.hadit.data.model.comment.Comment
import br.com.aguiar.matheus.hadit.data.model.comment.CommentChildren
import br.com.aguiar.matheus.hadit.data.model.comment.DataComment
import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.model.post.Post
import br.com.aguiar.matheus.hadit.data.model.post.PostData
import br.com.aguiar.matheus.hadit.data.model.post.RedditPost

val commentList = listOf(
    RedditComments(
        DataComment(
            listOf(
                CommentChildren(
                    Comment(
                        titleComment = "Titulo 1",
                        author = "Autor",
                        commentPost = "comentario",
                        votesPositive = 10
                    )
                )
            )
        )
    ),
    RedditComments(
        DataComment(
            listOf(
                CommentChildren(
                    Comment(
                        titleComment = "Titulo 2",
                        author = "Autor2",
                        commentPost = "comentario2",
                        votesPositive = -20
                    )
                )
            )
        )
    ),
    RedditComments(
        DataComment(
            listOf(
                CommentChildren(
                    Comment(
                        titleComment = "Titulo 3",
                        author = "Autor3",
                        commentPost = "comentario3",
                        votesPositive = 5
                    )
                )
            )
        )
    ),
    RedditComments(
        DataComment(
            listOf(
                CommentChildren(
                    Comment(
                        titleComment = "Titulo 4",
                        author = "Autor4",
                        commentPost = "comentario4",
                        votesPositive = 10000
                    )
                )
            )
        )
    )
)

val postList = RedditPost(
    PostData(
        listOf(
            DataChildren(
                Post(
                    title = "Titulo 1",
                    author = "Autor 1",
                    authorText = "mensagem",
                    created = 123123,
                    details = "",
                    link = "www.reddit.com/r/1",
                    numComments = 1000
                )
            ),
            DataChildren(
                Post(
                    title = "Titulo 2",
                    author = "Autor 2",
                    authorText = "mensagem 2",
                    created = 321312,
                    details = "",
                    link = "www.reddit.com/r/2",
                    numComments = 200
                )
            ),
            DataChildren(
                Post(
                    title = "Titulo 3",
                    author = "Autor 3",
                    authorText = "mensagem 3",
                    created = 231313,
                    details = "",
                    link = "www.reddit.com/r/3",
                    numComments = 4000
                )
            )
        )
    )
)