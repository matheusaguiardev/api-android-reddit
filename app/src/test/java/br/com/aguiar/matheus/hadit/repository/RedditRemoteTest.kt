package br.com.aguiar.matheus.hadit.repository

import br.com.aguiar.matheus.hadit.data.model.comment.RedditComments
import br.com.aguiar.matheus.hadit.data.model.post.DataChildren
import br.com.aguiar.matheus.hadit.data.model.post.RedditPost
import br.com.aguiar.matheus.hadit.data.outcome.Outcome
import br.com.aguiar.matheus.hadit.data.reddit.RedditRemote
import br.com.aguiar.matheus.hadit.data.reddit.RedditRepositoryImpl
import br.com.aguiar.matheus.hadit.testhelper.commentList
import br.com.aguiar.matheus.hadit.testhelper.postList
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import retrofit2.Response

class RedditRemoteTest {

    private var expectedCommentsList: Outcome<List<RedditComments>>? = null
    private var expectedPosts: Outcome<List<DataChildren>>? = null
    private val api = mock<RedditRemote>()
    private val repository = RedditRepositoryImpl(api)
    private val mediaType by lazy { MediaType.parse("application/json") }

    @Before
    fun setUp() {
        expectedCommentsList = null
    }

    @Test
    fun `baixar lista de comentarios de uma postagem`() = runBlocking {
        `dado que eu tenha uma lista de comentarios para baixar`(commentList)
        `quando eu executar o metodo de chamada de busca os comentarios do servico`()
        `entao verificar se o metodo que executa a logica retornar uma lista de comentarios`()
    }

    @Test
    fun `baixar lista de comentarios porem ocorre algum erro antes ou durante a requisicao`() =
        runBlocking {
            `dado que eu tenha uma lista de comentarios para baixar e um erro e esperado`()
            `quando eu executar o metodo de chamada de busca de comentarios do servico com erro`()
            `entao verificar se a mensagem retornada é de não encontrada e quando houver qualquer erro de request`()
        }

    @Test
    fun `baixar lista de postagens`() = runBlocking {
        `dado que eu tenha uma lista de postagens para baixar`(postList)
        `quando eu executar o metodo de chamada de busca as postagens do servico`()
        `entao verificar se o metodo que executa a logica retornar uma lista de postagens`()
    }

    @Test
    fun `baixar lista de postagens porem ocorre algum erro antes ou durante a requisicao`() =
        runBlocking {
            `dado que eu tenha uma lista de postagens para baixar e ocorre um erro de nao encontrado`()
            `quando eu executar o metodo de chamada de busca as postagens do servico e ocorrer algum erro`()
            `entao verificar se a mensagem retornada e de nao encontrada e quando houver qualquer erro de request ao tentar encontrar as postagens`()
        }

    /*************** DADO ******************/
    private fun `dado que eu tenha uma lista de comentarios para baixar`(list: List<RedditComments>) =
        runBlocking {
            whenever(api.commentsAsync(ArgumentMatchers.anyString()))
                .thenReturn(async { Response.success(list) })
        }

    private fun `dado que eu tenha uma lista de comentarios para baixar e um erro e esperado`() =
        runBlocking {
            whenever(api.commentsAsync(ArgumentMatchers.anyString()))
                .thenReturn(async {
                    Response.error<List<RedditComments>>(
                        500,
                        ResponseBody.create(mediaType, "")
                    )
                })
        }

    private fun `dado que eu tenha uma lista de postagens para baixar`(list: RedditPost) =
        runBlocking {
            whenever(api.homeAsync())
                .thenReturn(async { Response.success(list) })
        }

    private fun `dado que eu tenha uma lista de postagens para baixar e ocorre um erro de nao encontrado`() =
        runBlocking {
            whenever(api.homeAsync())
                .thenReturn(async {
                    Response.error<RedditPost>(
                        404,
                        ResponseBody.create(mediaType, "")
                    )
                })
        }

    /*************** QUANDO ******************/
    private suspend fun `quando eu executar o metodo de chamada de busca os comentarios do servico`() {
        expectedCommentsList = repository.getComments("www.testereddit.com.br")
    }

    private suspend fun `quando eu executar o metodo de chamada de busca de comentarios do servico com erro`() {
        expectedCommentsList = repository.getComments("www.testereddit.com.br")
    }

    private suspend fun `quando eu executar o metodo de chamada de busca as postagens do servico`() {
        expectedPosts = repository.getHome()
    }

    private suspend fun `quando eu executar o metodo de chamada de busca as postagens do servico e ocorrer algum erro`() {
        expectedPosts = repository.getHome()
    }

    /*************** ENTÃO ******************/
    private fun `entao verificar se o metodo que executa a logica retornar uma lista de comentarios`() {
        Assert.assertTrue(expectedCommentsList is Outcome.Success)
        Assert.assertTrue((expectedCommentsList as Outcome.Success).data.isNotEmpty())
    }

    private fun `entao verificar se a mensagem retornada é de não encontrada e quando houver qualquer erro de request`() {
        Assert.assertTrue(expectedCommentsList is Outcome.Error)
        Assert.assertTrue((expectedCommentsList as Outcome.Error).httpCode == 500)
        Assert.assertTrue((expectedCommentsList as Outcome.Error).exception.message == RedditRepositoryImpl.DATA_NOT_FOUND)
    }

    private fun `entao verificar se o metodo que executa a logica retornar uma lista de postagens`() {
        Assert.assertTrue(expectedPosts is Outcome.Success)
        Assert.assertTrue((expectedPosts as Outcome.Success).data.isNotEmpty())
    }

    private fun `entao verificar se a mensagem retornada e de nao encontrada e quando houver qualquer erro de request ao tentar encontrar as postagens`() {
        Assert.assertTrue(expectedPosts is Outcome.Error)
        Assert.assertTrue((expectedPosts as Outcome.Error).httpCode == 404)
        Assert.assertTrue((expectedPosts as Outcome.Error).exception.message == RedditRepositoryImpl.DATA_NOT_FOUND)
    }

}